import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Test;


/**
 * 
 * @author alejandro
 *
 */
public class FibonacciTest {

	@Test
	public void test() {
		Fibonacci fibon = new Fibonacci();
		try {
			Assert.assertEquals(12.0,fibon.fibo(0),0.005);
			Assert.assertEquals(12.0,fibon.fibo(1),0.005);
			Assert.assertEquals(12.0,fibon.fibo(5),0.005);
			Assert.assertEquals(12.0,fibon.fibo(30),0.005);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
