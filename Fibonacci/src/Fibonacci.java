//Comentario
public class Fibonacci {
	public double fibo (int n) throws Exception{
		if(n<0){
			throw new Exception();
		}
		if(n==0){
			return 0;
		}else if(n==1){
			return 1;
		}else{
			return fibo(n-1) + fibo(n-2);
		}
	}
}
